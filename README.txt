USDA Plant Data
===============

This module provides nodes containing plant data based on the USDA database at plants.usda.gov.
The browseable and ASCII database generation tool is found at: http://plants.usda.gov/adv_search.html

The module comes with a default CSV database including various plant fields.

On installation, this module loads the CSV and builds plant data fields and nodes from the provided data.
